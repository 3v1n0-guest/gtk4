<!--
SPDX-FileCopyrightText: 2021 GNOME Foundation

SPDX-License-Identifier: Apache-2.0 OR GPL-3.0-or-later
-->

<!--
SPDX-FileCopyrightText: 2021 GNOME Foundation

SPDX-License-Identifier: Apache-2.0 OR GPL-3.0-or-later
-->

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Gtk.LayoutManager</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta charset="utf-8" />

  
  <meta property="og:type" content="website"/>

  
  <meta property="og:image:width" content="256"/>
  <meta property="og:image:height" content="256"/>
  <meta property="og:image:secure_url" content="gtk-logo.svg"/>
  <meta property="og:image:alt" content="Gtk-4.0"/>
  

  
  <meta property="og:title" content="Gtk.LayoutManager"/>
  <meta property="og:description" content="Reference for Gtk.LayoutManager"/>
  <meta name="twitter:title" content="Gtk.LayoutManager"/>
  <meta name="twitter:description" content="Reference for Gtk.LayoutManager"/>


  
  <meta name="twitter:card" content="summary"/>

  

  <link rel="stylesheet" href="style.css" type="text/css" />

  

  
  <script src="urlmap.js"></script>
  
  
  <script src="fzy.js"></script>
  <script src="search.js"></script>
  
  <script src="main.js"></script>
  <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>

<body>
  <div id="body-wrapper" tabindex="-1">

    <nav class="sidebar devhelp-hidden">
      
      <div class="section">
        <img src="gtk-logo.svg" class="logo"/>
      </div>
      
      
      <div class="search section">
        <form id="search-form" autocomplete="off">
          <input id="search-input" type="text" name="do-not-autocomplete" placeholder="Click, or press 's' to search" autocomplete="off"/>
        </form>
      </div>
      
      <div class="section namespace">
        <h3><a href="index.html">Gtk</a></h3>
        <p>API Version: 4.0</p>
        
        <p>Library Version: 4.3.1</p>
        
      </div>
      
<div class="section">
  <h5>Type</h5>
  <div class="links">
    <a class="current" href="class.LayoutManager.html#description">LayoutManager</a>
  </div>
</div>




<div class="section">
  <h5>Instance methods</h5>
  <div class="links">
  
    <a class="method" href="method.LayoutManager.allocate.html">allocate</a>
  
    <a class="method" href="method.LayoutManager.get_layout_child.html">get_layout_child</a>
  
    <a class="method" href="method.LayoutManager.get_request_mode.html">get_request_mode</a>
  
    <a class="method" href="method.LayoutManager.get_widget.html">get_widget</a>
  
    <a class="method" href="method.LayoutManager.layout_changed.html">layout_changed</a>
  
    <a class="method" href="method.LayoutManager.measure.html">measure</a>
  
  </div>
</div>











<div class="section">
  <h5>Virtual methods</h5>
  <div class="links">
  
    <a class="method" href="vfunc.LayoutManager.allocate.html">allocate</a>
  
    <a class="method" href="vfunc.LayoutManager.create_layout_child.html">create_layout_child</a>
  
    <a class="method" href="vfunc.LayoutManager.get_request_mode.html">get_request_mode</a>
  
    <a class="method" href="vfunc.LayoutManager.measure.html">measure</a>
  
    <a class="method" href="vfunc.LayoutManager.root.html">root</a>
  
    <a class="method" href="vfunc.LayoutManager.unroot.html">unroot</a>
  
  </div>
</div>



    </nav>

    
    <button id="btn-to-top" class="hidden"><span class="up-arrow"></span></button>
    

    
<section id="main" class="content">
  <header>
    <h3>Class</h3>
    <h1 aria-label="Name"><a href="index.html">Gtk</a><span class="sep" role="presentation"></span>LayoutManager</h1>
  </header>

  <section>
    <summary>
      <div class="toggle-wrapper">
        <h4 id="description" style="display:flex;">
          Description
          <a href="#description" class="anchor"></a>
          
          <a class="srclink" title="go to source location" href="https://gitlab.gnome.org/GNOME/gtk/-/blob/master/gtk/gtklayoutmanager.c#L20">[src]</a>
          
        </h4>

        <pre><code>abstract class Gtk.LayoutManager : GObject.Object {
  parent_instance: GObject
}</pre></code>

        <div class="docblock">
          <p>Layout managers are delegate classes that handle the preferred size
and the allocation of a&nbsp;widget.</p>
<p>You typically subclass <code>GtkLayoutManager</code> if you want to implement a
layout policy for the children of a widget, or if you want to determine
the size of a widget depending on its&nbsp;contents.</p>
<p>Each <code>GtkWidget</code> can only have a <code>GtkLayoutManager</code> instance associated
to it at any given time; it is possible, though, to replace the layout
manager instance using <a href="method.Widget.set_layout_manager.html">gtk_widget_set_layout_manager()</a>.</p>
<h2 id="layout-properties">Layout properties<a class="md-anchor" href="#layout-properties" title="Permanent link"></a></h2>
<p>A layout manager can expose properties for controlling the layout of
each child, by creating an object type derived from <a href="class.LayoutChild.html"><code>GtkLayoutChild</code></a>
and installing the properties on it as normal <code>GObject</code> properties.</p>
<p>Each <code>GtkLayoutChild</code> instance storing the layout properties for a
specific child is created through the <a href="method.LayoutManager.get_layout_child.html">gtk_layout_manager_get_layout_child()</a>
method; a <code>GtkLayoutManager</code> controls the creation of its <code>GtkLayoutChild</code>
instances by overriding the GtkLayoutManagerClass.create_layout_child()
virtual function. The typical implementation should look&nbsp;like:</p>
<div class="codehilite"><pre><span></span><code><span class="k">static</span> <span class="n">GtkLayoutChild</span> <span class="o">*</span>
<span class="n">create_layout_child</span> <span class="p">(</span><span class="n">GtkLayoutManager</span> <span class="o">*</span><span class="n">manager</span><span class="p">,</span>
                     <span class="n">GtkWidget</span>        <span class="o">*</span><span class="n">container</span><span class="p">,</span>
                     <span class="n">GtkWidget</span>        <span class="o">*</span><span class="n">child</span><span class="p">)</span>
<span class="p">{</span>
  <span class="k">return</span> <span class="nf">g_object_new</span> <span class="p">(</span><span class="n">your_layout_child_get_type</span> <span class="p">(),</span>
                       <span class="s">&quot;layout-manager&quot;</span><span class="p">,</span> <span class="n">manager</span><span class="p">,</span>
                       <span class="s">&quot;child-widget&quot;</span><span class="p">,</span> <span class="n">child</span><span class="p">,</span>
                       <span class="nb">NULL</span><span class="p">);</span>
<span class="p">}</span>
</code></pre></div>

<p>The <a href="property.LayoutChild.layout-manager.html"><code>GtkLayoutChild:layout-manager</code></a> and
<a href="property.LayoutChild.child-widget.html"><code>GtkLayoutChild:child-widget</code></a> properties
on the newly created <code>GtkLayoutChild</code> instance are mandatory. The
<code>GtkLayoutManager</code> will cache the newly created <code>GtkLayoutChild</code> instance
until the widget is removed from its parent, or the parent removes the
layout&nbsp;manager.</p>
<p>Each <code>GtkLayoutManager</code> instance creating a <code>GtkLayoutChild</code> should use
<a href="method.LayoutManager.get_layout_child.html">gtk_layout_manager_get_layout_child()</a> every time it needs to query
the layout properties; each <code>GtkLayoutChild</code> instance should call
<a href="method.LayoutManager.layout_changed.html">gtk_layout_manager_layout_changed()</a> every time a property is
updated, in order to queue a new size measuring and&nbsp;allocation.</p>
        </div>

        <div class="docblock">
          <table class="attributes">
            
            
            
          </table>
        </div>

        
      </div>
    </summary>

    
    <div class="toggle-wrapper hierarchy">
      <h4 id="hierarchy">
        Hierarchy
        <a href="#hierarchy" class="anchor"></a>
      </h4>
      <div class="docblock" alt="Hierarchy for Gtk.LayoutManager">
        <?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"
 "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<!-- Generated by graphviz version 2.44.0 (0)
 -->
<!-- Title: hierarchy Pages: 1 -->
<svg width="156pt" height="116pt"
 viewBox="0.00 0.00 156.00 116.00" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g id="graph0" class="graph" transform="scale(1 1) rotate(0) translate(4 112)">
<title>hierarchy</title>
<!-- this -->
<g id="node1" class="node">
<title>this</title>
<g id="a_node1"><a xlink:title="GtkLayoutManager">
<path fill="none" stroke="black" d="M136,-36C136,-36 12,-36 12,-36 6,-36 0,-30 0,-24 0,-24 0,-12 0,-12 0,-6 6,0 12,0 12,0 136,0 136,0 142,0 148,-6 148,-12 148,-12 148,-24 148,-24 148,-30 142,-36 136,-36"/>
<text text-anchor="middle" x="74" y="-14.3" font-family="Times-Roman" font-size="14.00">GtkLayoutManager</text>
</a>
</g>
</g>
<!-- ancestor_0 -->
<g id="node2" class="node">
<title>ancestor_0</title>
<g id="a_node2"><a xlink:title="GObject.Object">
<path fill="none" stroke="black" d="M98,-108C98,-108 50,-108 50,-108 44,-108 38,-102 38,-96 38,-96 38,-84 38,-84 38,-78 44,-72 50,-72 50,-72 98,-72 98,-72 104,-72 110,-78 110,-84 110,-84 110,-96 110,-96 110,-102 104,-108 98,-108"/>
<text text-anchor="middle" x="74" y="-86.3" font-family="Times-Roman" font-size="14.00">GObject</text>
</a>
</g>
</g>
<!-- ancestor_0&#45;&#45;this -->
<g id="edge1" class="edge">
<title>ancestor_0&#45;&#45;this</title>
<path fill="none" stroke="black" d="M74,-71.7C74,-60.85 74,-46.92 74,-36.1"/>
</g>
</g>
</svg>

      </div>
    </div>
    

    
    <div class="toggle-wrapper ancestors">
      <h4 id="ancestors">
        Ancestors
        <a href="#ancestors" class="anchor"></a>
      </h4>

      <div class="docblock">
        <ul>
        
          
          <li class="class">GObject</a></li>
          
        
        </ul>
      </div>
    </div>
    

    

    

    
    <div class="toggle-wrapper methods">
      <h4 id="methods">
        Instance methods
        <a href="#methods" class="anchor"></a>
      </h4>

      <div class="docblock">
      
        <div class="">
          <h6><a href="method.LayoutManager.allocate.html">gtk_layout_manager_allocate</a></h6>
          <div class="docblock">
            <p>Assigns the given <code>width</code>, <code>height</code>, and <code>baseline</code> to
a <code>widget</code>, and computes the position and sizes of the children of
the <code>widget</code> using the layout management policy of <code>manager</code>.</p>
          </div>
          
        </div>
      
        <div class="">
          <h6><a href="method.LayoutManager.get_layout_child.html">gtk_layout_manager_get_layout_child</a></h6>
          <div class="docblock">
            <p>Retrieves a <code>GtkLayoutChild</code> instance for the <code>GtkLayoutManager</code>,
creating one if&nbsp;necessary.</p>
          </div>
          
        </div>
      
        <div class="">
          <h6><a href="method.LayoutManager.get_request_mode.html">gtk_layout_manager_get_request_mode</a></h6>
          <div class="docblock">
            <p>Retrieves the request mode of <code>manager</code>.</p>
          </div>
          
        </div>
      
        <div class="">
          <h6><a href="method.LayoutManager.get_widget.html">gtk_layout_manager_get_widget</a></h6>
          <div class="docblock">
            <p>Retrieves the <code>GtkWidget</code> using the given <code>GtkLayoutManager</code>.</p>
          </div>
          
        </div>
      
        <div class="">
          <h6><a href="method.LayoutManager.layout_changed.html">gtk_layout_manager_layout_changed</a></h6>
          <div class="docblock">
            <p>Queues a resize on the <code>GtkWidget</code> using <code>manager</code>, if&nbsp;any.</p>
          </div>
          
        </div>
      
        <div class="">
          <h6><a href="method.LayoutManager.measure.html">gtk_layout_manager_measure</a></h6>
          <div class="docblock">
            <p>Measures the size of the <code>widget</code> using <code>manager</code>, for the
given <code>orientation</code> and&nbsp;size.</p>
          </div>
          
        </div>
      
      </div>

      
        
      

      
    </div>
    

    

    

    
    <div class="class toggle-wrapper default-hide">
      <h4 id="class-struct">
        Class structure
        <a href="#class-struct" class="anchor"></a>
      </h4>

      <div class="docblock">
        <pre><code>struct GtkLayoutManagerClass {
  GtkSizeRequestMode (* get_request_mode) (
    GtkLayoutManager* manager,
    GtkWidget* widget
  );
  void (* measure) (
    GtkLayoutManager* manager,
    GtkWidget* widget,
    GtkOrientation orientation,
    int for_size,
    int* minimum,
    int* natural,
    int* minimum_baseline,
    int* natural_baseline
  );
  void (* allocate) (
    GtkLayoutManager* manager,
    GtkWidget* widget,
    int width,
    int height,
    int baseline
  );
  GType layout_child_type;
  GtkLayoutChild* (* create_layout_child) (
    GtkLayoutManager* manager,
    GtkWidget* widget,
    GtkWidget* for_child
  );
  void (* root) (
    GtkLayoutManager* manager
  );
  void (* unroot) (
    GtkLayoutManager* manager
  );
  
}</code></pre>
      </div>

      
      <div class="docblock">
        <h6>Class members</h6>

        <table class="members">
        
        <tr>
          <td style="vertical-align:top"><code>get_request_mode</code></td>
          <td style="vertical-align:top"><pre><code>GtkSizeRequestMode (* get_request_mode) (
    GtkLayoutManager* manager,
    GtkWidget* widget
  )</code></pre></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td style="vertical-align:top">No description available.</td>
        </td>
        
        <tr>
          <td style="vertical-align:top"><code>measure</code></td>
          <td style="vertical-align:top"><pre><code>void (* measure) (
    GtkLayoutManager* manager,
    GtkWidget* widget,
    GtkOrientation orientation,
    int for_size,
    int* minimum,
    int* natural,
    int* minimum_baseline,
    int* natural_baseline
  )</code></pre></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td style="vertical-align:top">No description available.</td>
        </td>
        
        <tr>
          <td style="vertical-align:top"><code>allocate</code></td>
          <td style="vertical-align:top"><pre><code>void (* allocate) (
    GtkLayoutManager* manager,
    GtkWidget* widget,
    int width,
    int height,
    int baseline
  )</code></pre></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td style="vertical-align:top">No description available.</td>
        </td>
        
        <tr>
          <td style="vertical-align:top"><code>layout_child_type</code></td>
          <td style="vertical-align:top"><pre><code>GType</code></pre></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td style="vertical-align:top"><p>The type of <code>GtkLayoutChild</code> used by this layout&nbsp;manager.</p></td>
        </td>
        
        <tr>
          <td style="vertical-align:top"><code>create_layout_child</code></td>
          <td style="vertical-align:top"><pre><code>GtkLayoutChild* (* create_layout_child) (
    GtkLayoutManager* manager,
    GtkWidget* widget,
    GtkWidget* for_child
  )</code></pre></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td style="vertical-align:top">No description available.</td>
        </td>
        
        <tr>
          <td style="vertical-align:top"><code>root</code></td>
          <td style="vertical-align:top"><pre><code>void (* root) (
    GtkLayoutManager* manager
  )</code></pre></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td style="vertical-align:top">No description available.</td>
        </td>
        
        <tr>
          <td style="vertical-align:top"><code>unroot</code></td>
          <td style="vertical-align:top"><pre><code>void (* unroot) (
    GtkLayoutManager* manager
  )</code></pre></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td style="vertical-align:top">No description available.</td>
        </td>
        
        </table>
      </div>
      
    </div>
    

    
    <div class="toggle-wrapper methods">
      <h4 id="virtual-methods">
        Virtual methods
        <a href="#virtual-methods" class="anchor"></a>
      </h4>

      <div class="docblock">
      
      <div class="">
        <h6 id="vfunc-allocate"><a href="vfunc.LayoutManager.allocate.html">Gtk.LayoutManagerClass.allocate</a></h6>

        <div class="docblock">
          <p>Assigns the given <code>width</code>, <code>height</code>, and <code>baseline</code> to
a <code>widget</code>, and computes the position and sizes of the children of
the <code>widget</code> using the layout management policy of <code>manager</code>.</p>
        </div>

          
      </div>
      
      <div class="">
        <h6 id="vfunc-create_layout_child"><a href="vfunc.LayoutManager.create_layout_child.html">Gtk.LayoutManagerClass.create_layout_child</a></h6>

        <div class="docblock">
          <p>Create a <code>GtkLayoutChild</code> instance for the given <code>for_child</code> widget.</p>
        </div>

          
      </div>
      
      <div class="">
        <h6 id="vfunc-get_request_mode"><a href="vfunc.LayoutManager.get_request_mode.html">Gtk.LayoutManagerClass.get_request_mode</a></h6>

        <div class="docblock">
          No description available.
        </div>

          
      </div>
      
      <div class="">
        <h6 id="vfunc-measure"><a href="vfunc.LayoutManager.measure.html">Gtk.LayoutManagerClass.measure</a></h6>

        <div class="docblock">
          <p>Measures the size of the <code>widget</code> using <code>manager</code>, for the
given <code>orientation</code> and&nbsp;size.</p>
        </div>

          
      </div>
      
      <div class="">
        <h6 id="vfunc-root"><a href="vfunc.LayoutManager.root.html">Gtk.LayoutManagerClass.root</a></h6>

        <div class="docblock">
          No description available.
        </div>

          
      </div>
      
      <div class="">
        <h6 id="vfunc-unroot"><a href="vfunc.LayoutManager.unroot.html">Gtk.LayoutManagerClass.unroot</a></h6>

        <div class="docblock">
          No description available.
        </div>

          
      </div>
      
      </div>
    </div>
    

     

    

  </section>
</section>


    <section id="search" class="content hidden"></section>

    <footer>
    
    </footer>
  </div>
</body>
</html>