<!--
SPDX-FileCopyrightText: 2021 GNOME Foundation

SPDX-License-Identifier: Apache-2.0 OR GPL-3.0-or-later
-->

<!--
SPDX-FileCopyrightText: 2021 GNOME Foundation

SPDX-License-Identifier: Apache-2.0 OR GPL-3.0-or-later
-->

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Gtk.SizeGroup</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta charset="utf-8" />

  
  <meta property="og:type" content="website"/>

  
  <meta property="og:image:width" content="256"/>
  <meta property="og:image:height" content="256"/>
  <meta property="og:image:secure_url" content="gtk-logo.svg"/>
  <meta property="og:image:alt" content="Gtk-4.0"/>
  

  
  <meta property="og:title" content="Gtk.SizeGroup"/>
  <meta property="og:description" content="Reference for Gtk.SizeGroup"/>
  <meta name="twitter:title" content="Gtk.SizeGroup"/>
  <meta name="twitter:description" content="Reference for Gtk.SizeGroup"/>


  
  <meta name="twitter:card" content="summary"/>

  

  <link rel="stylesheet" href="style.css" type="text/css" />

  

  
  <script src="urlmap.js"></script>
  
  
  <script src="fzy.js"></script>
  <script src="search.js"></script>
  
  <script src="main.js"></script>
  <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>

<body>
  <div id="body-wrapper" tabindex="-1">

    <nav class="sidebar devhelp-hidden">
      
      <div class="section">
        <img src="gtk-logo.svg" class="logo"/>
      </div>
      
      
      <div class="search section">
        <form id="search-form" autocomplete="off">
          <input id="search-input" type="text" name="do-not-autocomplete" placeholder="Click, or press 's' to search" autocomplete="off"/>
        </form>
      </div>
      
      <div class="section namespace">
        <h3><a href="index.html">Gtk</a></h3>
        <p>API Version: 4.0</p>
        
        <p>Library Version: 4.3.1</p>
        
      </div>
      
<div class="section">
  <h5>Type</h5>
  <div class="links">
    <a class="current" href="class.SizeGroup.html#description">SizeGroup</a>
  </div>
</div>


<div class="section">
  <h5>Constructors</h5>
  <div class="links">
  
    <a class="ctor" href="ctor.SizeGroup.new.html">new</a>
  
  </div>
</div>



<div class="section">
  <h5>Instance methods</h5>
  <div class="links">
  
    <a class="method" href="method.SizeGroup.add_widget.html">add_widget</a>
  
    <a class="method" href="method.SizeGroup.get_mode.html">get_mode</a>
  
    <a class="method" href="method.SizeGroup.get_widgets.html">get_widgets</a>
  
    <a class="method" href="method.SizeGroup.remove_widget.html">remove_widget</a>
  
    <a class="method" href="method.SizeGroup.set_mode.html">set_mode</a>
  
  </div>
</div>



<div class="section">
  <h5>Properties</h5>
  <div class="links">
  
    <a class="property" href="property.SizeGroup.mode.html">mode</a>
  
  </div>
</div>











    </nav>

    
    <button id="btn-to-top" class="hidden"><span class="up-arrow"></span></button>
    

    
<section id="main" class="content">
  <header>
    <h3>Class</h3>
    <h1 aria-label="Name"><a href="index.html">Gtk</a><span class="sep" role="presentation"></span>SizeGroup</h1>
  </header>

  <section>
    <summary>
      <div class="toggle-wrapper">
        <h4 id="description" style="display:flex;">
          Description
          <a href="#description" class="anchor"></a>
          
          <a class="srclink" title="go to source location" href="https://gitlab.gnome.org/GNOME/gtk/-/blob/master/gtk/gtksizegroup.c#L32">[src]</a>
          
        </h4>

        <pre><code>final class Gtk.SizeGroup : GObject.Object {
  parent_instance: GObject
}</pre></code>

        <div class="docblock">
          <p><code>GtkSizeGroup</code> groups widgets together so they all request the same&nbsp;size.</p>
<p>This is typically useful when you want a column of widgets to have the
same size, but you can’t use a <code>GtkGrid</code>.</p>
<p>In detail, the size requested for each widget in a <code>GtkSizeGroup</code> is
the maximum of the sizes that would have been requested for each
widget in the size group if they were not in the size group. The mode
of the size group (see <a href="method.SizeGroup.set_mode.html">gtk_size_group_set_mode()</a>) determines whether
this applies to the horizontal size, the vertical size, or both&nbsp;sizes.</p>
<p>Note that size groups only affect the amount of space requested, not
the size that the widgets finally receive. If you want the widgets in
a <code>GtkSizeGroup</code> to actually be the same size, you need to pack them in
such a way that they get the size they request and not&nbsp;more.</p>
<p><code>GtkSizeGroup</code> objects are referenced by each widget in the size group,
so once you have added all widgets to a <code>GtkSizeGroup</code>, you can drop
the initial reference to the size group with g_object_unref(). If the
widgets in the size group are subsequently destroyed, then they will
be removed from the size group and drop their references on the size
group; when all widgets have been removed, the size group will be&nbsp;freed.</p>
<p>Widgets can be part of multiple size groups; <span class="caps">GTK</span> will compute the
horizontal size of a widget from the horizontal requisition of all
widgets that can be reached from the widget by a chain of size groups
of type <code>GTK_SIZE_GROUP_HORIZONTAL</code> or <code>GTK_SIZE_GROUP_BOTH</code>, and the
vertical size from the vertical requisition of all widgets that can be
reached from the widget by a chain of size groups of type
<code>GTK_SIZE_GROUP_VERTICAL</code> or <code>GTK_SIZE_GROUP_BOTH</code>.</p>
<p>Note that only non-contextual sizes of every widget are ever consulted
by size groups (since size groups have no knowledge of what size a widget
will be allocated in one dimension, it cannot derive how much height
a widget will receive for a given width). When grouping widgets that
trade height for width in mode <code>GTK_SIZE_GROUP_VERTICAL</code> or <code>GTK_SIZE_GROUP_BOTH</code>:
the height for the minimum width will be the requested height for all
widgets in the group. The same is of course true when horizontally grouping
width for height&nbsp;widgets.</p>
<p>Widgets that trade height-for-width should set a reasonably large minimum
width by way of <a href="property.Label.width-chars.html"><code>GtkLabel:width-chars</code></a> for instance. Widgets with
static sizes as well as widgets that grow (such as ellipsizing text) need no
such&nbsp;considerations.</p>
<h1 id="gtksizegroup-as-gtkbuildable">GtkSizeGroup as GtkBuildable<a class="md-anchor" href="#gtksizegroup-as-gtkbuildable" title="Permanent link"></a></h1>
<p>Size groups can be specified in a <span class="caps">UI</span> definition by placing an <object>
element with <code>class="GtkSizeGroup"</code> somewhere in the <span class="caps">UI</span> definition. The
widgets that belong to the size group are specified by a <widgets> element
that may contain multiple <widget> elements, one for each member of the
size group. The ”name” attribute gives the id of the&nbsp;widget.</p>
<p>An example of a <span class="caps">UI</span> definition fragment with <code>GtkSizeGroup</code>:&nbsp;&#8220;`xml</p>
<object class="GtkSizeGroup">
  <property name="mode">horizontal</property>
  <widgets>
    <widget name="radio1"/>
    <widget name="radio2"/>
  </widgets>
</object>
<p><span class="dquo">&#8220;</span>`.</p>
        </div>

        <div class="docblock">
          <table class="attributes">
            
            
            
          </table>
        </div>

        
      </div>
    </summary>

    
    <div class="toggle-wrapper hierarchy">
      <h4 id="hierarchy">
        Hierarchy
        <a href="#hierarchy" class="anchor"></a>
      </h4>
      <div class="docblock" alt="Hierarchy for Gtk.SizeGroup">
        <?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"
 "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<!-- Generated by graphviz version 2.44.0 (0)
 -->
<!-- Title: hierarchy Pages: 1 -->
<svg width="119pt" height="188pt"
 viewBox="0.00 0.00 119.00 188.00" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g id="graph0" class="graph" transform="scale(1 1) rotate(0) translate(4 184)">
<title>hierarchy</title>
<!-- this -->
<g id="node1" class="node">
<title>this</title>
<g id="a_node1"><a xlink:title="GtkSizeGroup">
<path fill="none" stroke="black" d="M99,-108C99,-108 12,-108 12,-108 6,-108 0,-102 0,-96 0,-96 0,-84 0,-84 0,-78 6,-72 12,-72 12,-72 99,-72 99,-72 105,-72 111,-78 111,-84 111,-84 111,-96 111,-96 111,-102 105,-108 99,-108"/>
<text text-anchor="middle" x="55.5" y="-86.3" font-family="Times-Roman" font-size="14.00">GtkSizeGroup</text>
</a>
</g>
</g>
<!-- implements_0 -->
<g id="node3" class="node link">
<title>implements_0</title>
<g id="a_node3"><a xlink:href="class.Buildable.html" xlink:title="GtkBuildable">
<polygon fill="none" stroke="black" points="108,-36 3,-36 3,0 108,0 108,-36"/>
<text text-anchor="middle" x="55.5" y="-14.3" font-family="sans-serif" font-size="14.00">GtkBuildable</text>
</a>
</g>
</g>
<!-- this&#45;&#45;implements_0 -->
<g id="edge2" class="edge">
<title>this&#45;&#45;implements_0</title>
<path fill="none" stroke="black" stroke-dasharray="1,5" d="M55.5,-71.7C55.5,-60.85 55.5,-46.92 55.5,-36.1"/>
</g>
<!-- ancestor_0 -->
<g id="node2" class="node">
<title>ancestor_0</title>
<g id="a_node2"><a xlink:title="GObject.Object">
<path fill="none" stroke="black" d="M79.5,-180C79.5,-180 31.5,-180 31.5,-180 25.5,-180 19.5,-174 19.5,-168 19.5,-168 19.5,-156 19.5,-156 19.5,-150 25.5,-144 31.5,-144 31.5,-144 79.5,-144 79.5,-144 85.5,-144 91.5,-150 91.5,-156 91.5,-156 91.5,-168 91.5,-168 91.5,-174 85.5,-180 79.5,-180"/>
<text text-anchor="middle" x="55.5" y="-158.3" font-family="Times-Roman" font-size="14.00">GObject</text>
</a>
</g>
</g>
<!-- ancestor_0&#45;&#45;this -->
<g id="edge1" class="edge">
<title>ancestor_0&#45;&#45;this</title>
<path fill="none" stroke="black" d="M55.5,-143.7C55.5,-132.85 55.5,-118.92 55.5,-108.1"/>
</g>
</g>
</svg>

      </div>
    </div>
    

    
    <div class="toggle-wrapper ancestors">
      <h4 id="ancestors">
        Ancestors
        <a href="#ancestors" class="anchor"></a>
      </h4>

      <div class="docblock">
        <ul>
        
          
          <li class="class">GObject</a></li>
          
        
        </ul>
      </div>
    </div>
    

    
    <div class="toggle-wrapper implements">
      <h4 id="implements">
        Implements
        <a href="#implements" class="anchor"></a>
      </h4>

      <div class="docblock">
        <ul>
        
          
          <li class="interface"><a href="iface.Buildable.html" title="Buildable">GtkBuildable</a></li>
          
        
        </ul>
      </div>
    </div>
    

    
    <div class="toggle-wrapper constructors">
      <h4 id="constructors">
        Constructors
        <a href="#constructors" class="anchor"></a>
      </h4>

      <div class="docblock">
      
        <div class="">
          <h6><a href="ctor.SizeGroup.new.html">gtk_size_group_new</a></h6>

          <div class="docblock">
            <p>Create a new <code>GtkSizeGroup</code>.</p>
          </div>

          
        </div>
      
      </div>
    </div>
    

    
    <div class="toggle-wrapper methods">
      <h4 id="methods">
        Instance methods
        <a href="#methods" class="anchor"></a>
      </h4>

      <div class="docblock">
      
        <div class="">
          <h6><a href="method.SizeGroup.add_widget.html">gtk_size_group_add_widget</a></h6>
          <div class="docblock">
            <p>Adds a widget to a <code>GtkSizeGroup</code>.</p>
          </div>
          
        </div>
      
        <div class="">
          <h6><a href="method.SizeGroup.get_mode.html">gtk_size_group_get_mode</a></h6>
          <div class="docblock">
            <p>Gets the current mode of the size&nbsp;group.</p>
          </div>
          
        </div>
      
        <div class="">
          <h6><a href="method.SizeGroup.get_widgets.html">gtk_size_group_get_widgets</a></h6>
          <div class="docblock">
            <p>Returns the list of widgets associated with <code>size_group</code>.</p>
          </div>
          
        </div>
      
        <div class="">
          <h6><a href="method.SizeGroup.remove_widget.html">gtk_size_group_remove_widget</a></h6>
          <div class="docblock">
            <p>Removes a widget from a <code>GtkSizeGroup</code>.</p>
          </div>
          
        </div>
      
        <div class="">
          <h6><a href="method.SizeGroup.set_mode.html">gtk_size_group_set_mode</a></h6>
          <div class="docblock">
            <p>Sets the <code>GtkSizeGroupMode</code> of the size&nbsp;group.</p>
          </div>
          
        </div>
      
      </div>

      
        
      

      
        
        <div class="toggle-wrapper default-hide ancestor-methods">
          <h5 style="display:block;">Methods inherited from <a href="iface.Buildable.html">GtkBuildable</a> (1)</h5>

          
          <div class="docblock">
          
            <h6><a href="method.Buildable.get_buildable_id.html">gtk_buildable_get_buildable_id</a></h6>
            <div class="docblock">
              <p>Gets the <span class="caps">ID</span> of the <code>buildable</code> object.</p>
            </div>
            
          
          </div>
          
        </div>
        
      
    </div>
    

    
    <div class="toggle-wrapper properties">
      <h4 id="properties">
        Properties
        <a href="#properties" class="anchor"></a>
      </h4>

      <div class="docblock">
      
        <div class="">
          <h6><a href="property.SizeGroup.mode.html">Gtk.SizeGroup:mode</a></h6>

          <div class="docblock">
            <p>The direction in which the size group affects requested&nbsp;sizes.</p>
          </div>

          
        </div>
      
      </div>

      
        
      

      
        
      

    </div>
    

    

    

    

     

    

  </section>
</section>


    <section id="search" class="content hidden"></section>

    <footer>
    
    </footer>
  </div>
</body>
</html>